# framenet_ros_msgs

This package is a collection of FrameNet frames [https://framenet.icsi.berkeley.edu/fndrupal/frameIndex] mapped to ROS messages, for a better integration of the linguistic theory into the ROS environment.

##Frames mapped

 * *Motion* --> `Motion` 
 * *Change_direction* --> `ChangeDirection`
 * *Bringing* --> `Bringing`